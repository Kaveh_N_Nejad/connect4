extends "res://addons/gut/test.gd"

var load_helper = preload("res://src/tests/helper_test_scripts/basic_loads.gd").new()

var start_screen
var change_scene_listener

class listen_to_change_scene:
	var chenge = false
	func _on_change_scene():
		chenge = true


func before_each():
	start_screen = autofree(load_helper.set_up_start_window())
	start_screen._ready()
	start_screen.disconnect("change_scene", start_screen, "_on_start_window_change_scene")
	change_scene_listener = listen_to_change_scene.new()
	start_screen.connect("change_scene", change_scene_listener, "_on_change_scene")


func test_cannot_start_game_both_names():
	start_screen.player2_name_input.text = start_screen.player1_name_input.text
	start_screen._on_Button_pressed()
	assert_false(change_scene_listener.chenge, "should not change scene when both playes have same name")


func test_start_game_does_not_work_when_either_players_name_is_empty():
	start_screen.player2_name_input.text = ""
	start_screen._on_Button_pressed()
	assert_false(change_scene_listener.chenge, "should not change scene when both playes have same name")
	
	start_screen.player2_name_input.text = "test"
	start_screen.player1_name_input.text = ""
	start_screen._on_Button_pressed()
	assert_false(change_scene_listener.chenge, "should not change scene when both playes have same name")


func test_start_game_does_not_work_when_both_players_name_is_empty():
	start_screen.player2_name_input.text = ""
	start_screen.player1_name_input.text = ""
	start_screen._on_Button_pressed()
	assert_false(change_scene_listener.chenge, "should not change scene when both playes have same name")


func test_player_names_start_diferent():
	assert_false(start_screen.player2_name_input.text == start_screen.player1_name_input.text, "player names should start of diferent")


func test_start_game_works_when_names_are_diferent():
	start_screen._on_Button_pressed()
	assert_false(start_screen.player2_name_input.text == start_screen.player1_name_input.text, "player names should start of diferent")
	assert_true(change_scene_listener.chenge, "should change scene when both playes are same name")
