extends Node

var start_window_script = preload("res://src/scenes/start_window.tscn")


func set_up_start_window():
	var start_window = start_window_script.instance()
	return start_window
