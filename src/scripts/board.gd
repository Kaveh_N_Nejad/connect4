extends Node2D

export(PackedScene) var tiles_scene
export(PackedScene) var player_scene

var check_for_match = load("res://src/scripts/check_for_match.gd").new()

onready var score_label = get_node("score")
onready var current_player_label = get_node("current_player_label")

var tiles = []
var tile_locations = {}

var players = []
var colors = ["ffff00", "ff0000"]
var wins = [0,0]

var h_container

var current_player

func _ready():
	_reset()
	_set_map()
	check_for_match.tiles_dict = tile_locations
	check_for_match.set_reverse_tiles_dict()
	check_for_match.board = self
	_set_players(Global.player_names)


func _reset():
	if h_container:
		h_container.queue_free()
	tiles = []
	tile_locations = {}
	players = []


func _set_players(names):
	for i in range(2):
		var player = player_scene.instance()
		players.append(player)
		player.color = colors[i]
		player.player_name = names[i]
	current_player = players.front()
	_update_current_player_label()


func _set_map():
	_set_tiles_list()
	_display_tiles()


func _set_tiles_list():
	for x in range(10):
		tiles.append([])
		for y in range(8):
			var tile = tiles_scene.instance()
			tile_locations[tile] = Vector2(x,y)
			tiles[x].append(tile)


func _display_tiles():
	h_container = HBoxContainer.new()
	add_child(h_container)
	for x_list in tiles:
		var vertical_constainer = VBoxContainer.new()
		var button = Button.new()
		button.text = "drop here"
		button.connect("pressed", self, "_on_button_clicked", [x_list])
		h_container.add_child(vertical_constainer)
		vertical_constainer.add_child(button)
		for tile in x_list:
			vertical_constainer.add_child(tile)


func _on_button_clicked(list):
	var tile = _get_next_possible_slot(list)
	if tile:
		tile.set_player(current_player)
		if not check_for_match.check_for_match(tiles, tile):
			_set_next_player()


func _get_next_possible_slot(list):
	for i in range(list.size() - 1, -1, -1):
		var tile = list[i]
		if not tile.used:
			return tile
	return false


func _set_next_player():
	var current_index = players.find(current_player)
	if current_index + 1 < players.size():
		current_player = players[current_index + 1]
	else:
		current_player = players.front()
	_update_current_player_label()

func _on_win():
	update_wins(current_player)
	_ready()


func update_wins(player):
	wins[players.find(player)] += 1
	var text = players[0].player_name + ": " + String(wins[0])
	text += "\n" + players[1].player_name + ": " + String(wins[1])
	score_label.text = text


func _update_current_player_label():
	current_player_label.text = "current player: " + current_player.player_name



func _on_reset_button_pressed():
	print(wins)
	_ready()
