extends Node

var board

var tiles_dict
var reverse_tiles_dict = {}

var tiles
var new_tile
var new_tile_location

var counts = [1,1,1,1]

var directions_to_counts_index_dict = {
	Vector2(-1,0) : 0,
	Vector2(1,0) : 0,
	Vector2(0,-1) : 1,
	Vector2(0,1) : 1,
	Vector2(1,1) : 2,
	Vector2(-1,-1) : 2,
	Vector2(1,-1) : 3,
	Vector2(-1,1) : 3,
	}


func check_for_match(new_tiles, next_tile):
	_set_vars(new_tiles, next_tile)
	for direction in directions_to_counts_index_dict.keys():
		counts[directions_to_counts_index_dict[direction]] += _check_line(direction)
	
	for count in counts:
		if count >= 4:
			board._on_win()
			return true
	return false


func _check_line(direction):
	var next_tile_to_check = _get_next_tile(direction, tiles_dict[new_tile])
	var diferent_player = false
	var count = 0
	while next_tile_to_check and not diferent_player:
		if next_tile_to_check.player == new_tile.player:
			count += 1
		else:
			diferent_player = true
		next_tile_to_check = _get_next_tile(direction, tiles_dict[next_tile_to_check])
	return count


func _get_next_tile(direction, previous_location):
	var next_location = previous_location + direction
	if next_location in reverse_tiles_dict.keys():
		return reverse_tiles_dict[next_location]
	return false


func set_reverse_tiles_dict():
	for key in tiles_dict.keys():
		reverse_tiles_dict[tiles_dict[key]] = key


func _set_vars(new_tiles, next_tile):
	counts = [1,1,1,1]
	self.tiles = new_tiles
	self.new_tile = next_tile
	self.new_tile_location = tiles_dict[new_tile]
