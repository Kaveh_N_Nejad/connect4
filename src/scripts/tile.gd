extends Control

onready var sprite = get_node("sprite")

var player = null
var used = false

func set_player(new_player):
	sprite.visible = true
	sprite.modulate = Color(new_player.color)
	player = new_player
	used = true
