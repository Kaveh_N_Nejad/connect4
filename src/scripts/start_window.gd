extends Node2D

signal change_scene

onready var player1_name_input  = get_node("player1_name_input")
onready var player2_name_input = get_node("player2_name_input")

var board_path = "res://src/scenes/board.tscn"



func _on_Button_pressed():
	var player1_name = player1_name_input.text
	var player2_name = player2_name_input.text
	if player1_name == "" or player2_name == "" or player1_name == player2_name:
		return
	Global.player_names.append(player1_name)
	Global.player_names.append(player2_name)
	emit_signal("change_scene")


func _on_start_window_change_scene():
	assert(get_tree().change_scene(board_path) == OK)
